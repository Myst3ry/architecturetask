package com.seriabov.fintecharch;

import android.app.Application;
import android.content.Context;

import com.seriabov.fintecharch.di.AppComponent;
import com.seriabov.fintecharch.di.DaggerAppComponent;

import timber.log.Timber;

public class AppDelegate extends Application {

    private AppComponent appComponent;

    public static AppDelegate from(Context context) {
        return (AppDelegate) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initDaggerAppComponent();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initDaggerAppComponent() {
        appComponent = DaggerAppComponent.create();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}

