package com.seriabov.fintecharch.ui.coins;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.seriabov.fintecharch.data.OnLoadCallback;
import com.seriabov.fintecharch.data.model.CoinInfo;
import com.seriabov.fintecharch.data.repository.CoinsRepository;

import java.util.List;

import timber.log.Timber;

public class MainViewModel extends ViewModel {

    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Boolean> isError;
    private MutableLiveData<List<CoinInfo>> data;

    private final CoinsRepository coinsRepository;

    public MainViewModel(CoinsRepository repository) {
        this.coinsRepository = repository;
    }

    public void requestNewData() {
        isLoading.setValue(true);
        coinsRepository.getCoins(new OnLoadCallback() {
            @Override
            public void onLoadSuccess(List<CoinInfo> coinInfoList) {
                Timber.d(String.valueOf(coinInfoList.size()));
                data.postValue(coinInfoList);
                isError.setValue(false);
                isLoading.setValue(false);
            }

            @Override
            public void onLoadFailure(Throwable throwable) {
                Timber.d(throwable);
                isError.setValue(true);
                isLoading.setValue(false);
            }
        });
    }

    public LiveData<Boolean> getLoading() {
        if (isLoading == null) {
            isLoading = new MutableLiveData<>();
        }
        return isLoading;
    }

    public LiveData<Boolean> getError() {
        if (isError == null) {
            isError = new MutableLiveData<>();
        }
        return isError;
    }

    public LiveData<List<CoinInfo>> getData() {
        if (data == null) {
            data = new MutableLiveData<>();
            requestNewData();
        }
        return data;
    }
}
