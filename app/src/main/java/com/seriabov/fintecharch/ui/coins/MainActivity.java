package com.seriabov.fintecharch.ui.coins;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.seriabov.fintecharch.AppDelegate;
import com.seriabov.fintecharch.R;
import com.seriabov.fintecharch.data.model.CoinInfo;
import com.seriabov.fintecharch.ui.coin_details.DetailsActivity;
import com.seriabov.fintecharch.util.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    ViewModelFactory factory;

    private MainViewModel viewModel;
    private CoinsAdapter adapter;
    private View errorView;
    private View contentView;
    private View loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppDelegate.from(this).getAppComponent().inject(this);

        setupToolbar();
        setupFab();

        initViews();
        initRecyclerView();

        prepareViewModel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            requestNewData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void prepareViewModel() {
        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        viewModel.getLoading().observe(this, this::showLoading);
        viewModel.getError().observe(this, this::showError);
        viewModel.getData().observe(this, this::setData);
    }

    private void requestNewData() {
        viewModel.requestNewData();
    }

    private void setData(List<CoinInfo> data) {
        adapter.setData(data);
    }

    private void showLoading(boolean isLoading) {
        loadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    private void showError(boolean isError) {
        contentView.setVisibility(isError ? View.GONE : View.VISIBLE);
        errorView.setVisibility(isError ? View.VISIBLE : View.GONE);
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setupFab() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> requestNewData());
    }

    private void initViews() {
        errorView = findViewById(R.id.error_layout);
        contentView = findViewById(R.id.main_recycler_view);
        loadingView = findViewById(R.id.loading_layout);
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.main_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CoinsAdapter(coinInfo -> DetailsActivity.start(MainActivity.this, coinInfo));
        recyclerView.setAdapter(adapter);
    }
}