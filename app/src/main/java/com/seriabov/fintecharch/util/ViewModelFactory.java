package com.seriabov.fintecharch.util;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.seriabov.fintecharch.data.repository.CoinsRepository;
import com.seriabov.fintecharch.ui.coins.MainViewModel;

public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final CoinsRepository coinsRepository;

    public ViewModelFactory(CoinsRepository repository) {
        this.coinsRepository = repository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            //noinspection unchecked
            return (T) new MainViewModel(coinsRepository);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
        }
    }
}
