package com.seriabov.fintecharch.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.seriabov.fintecharch.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    private Utils() {
    }

    public static int getChangeTextColor(Context context, double change) {
        return ContextCompat.getColor(context, change > 0 ? R.color.green700 : R.color.red700);
    }

    public static String getLastUpdatedDate(long dateInMillis) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault());
        Date date = new Date(dateInMillis * 1000);
        return dateFormat.format(date);
    }
}
