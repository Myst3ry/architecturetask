package com.seriabov.fintecharch.data.repository;

import android.support.annotation.NonNull;

import com.seriabov.fintecharch.data.OnLoadCallback;
import com.seriabov.fintecharch.data.model.CoinInfo;
import com.seriabov.fintecharch.data.remote.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoinsRepositoryImpl implements CoinsRepository {

    private final Api apiService;

    public CoinsRepositoryImpl(Api apiService) {
        this.apiService = apiService;
    }

    @Override
    public void getCoins(OnLoadCallback callback) {
        apiService.getCoinsList().enqueue(new Callback<List<CoinInfo>>() {
            @Override
            public void onResponse(@NonNull Call<List<CoinInfo>> call, @NonNull Response<List<CoinInfo>> response) {
                callback.onLoadSuccess(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<CoinInfo>> call, @NonNull Throwable t) {
                callback.onLoadFailure(t);
            }
        });
    }
}
