package com.seriabov.fintecharch.data.repository;

import com.seriabov.fintecharch.data.OnLoadCallback;

public interface CoinsRepository {

    void getCoins(OnLoadCallback callback);
}
