package com.seriabov.fintecharch.data;

import com.seriabov.fintecharch.data.model.CoinInfo;

import java.util.List;

public interface OnLoadCallback {

    void onLoadSuccess(List<CoinInfo> coinInfoList);

    void onLoadFailure(Throwable throwable);
}
