package com.seriabov.fintecharch.di;

import com.seriabov.fintecharch.ui.coins.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);
}
