package com.seriabov.fintecharch.di;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.seriabov.fintecharch.BuildConfig;
import com.seriabov.fintecharch.data.remote.Api;
import com.seriabov.fintecharch.data.repository.CoinsRepository;
import com.seriabov.fintecharch.data.repository.CoinsRepositoryImpl;
import com.seriabov.fintecharch.util.ViewModelFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @Provides
    @Singleton
    ViewModelFactory providesViewModelFactory(CoinsRepository coinsRepository) {
        return new ViewModelFactory(coinsRepository);
    }

    @Provides
    @Singleton
    CoinsRepository providesCoinsRepository(Api apiService) {
        return new CoinsRepositoryImpl(apiService);
    }

    @Provides
    @Singleton
    Api providesApiService(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }
}
